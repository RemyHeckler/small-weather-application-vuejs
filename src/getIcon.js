const _ = require('lodash')

const iconArray = ['wi wi-tornado', 'wi wi-storm-showers', 'wi wi-hurricane', 'wi wi-thunderstorm',
  'wi wi-thunderstorm', 'wi wi-rain-mix', 'wi wi-sleet', 'wi wi-sleet', 'wi wi-snowflake-cold',
  'wi wi-snowflake-cold', 'wi wi-rain-mix', 'wi wi-showers', 'wi wi-showers', 'wi wi-snow', 'wi wi-snow',
  'wi wi-snow', 'wi wi-snow', 'wi wi-hail', 'wi wi-sleet', 'wi wi-dust', 'wi wi-fog', 'wi wi-day-haze',
  'wi wi-smoke', 'wi wi-sandstorm', 'wi wi-windy', 'wi wi-snowflake-cold', 'wi wi-cloudy',
  'wi wi-night-alt-cloudy', 'wi wi-day-cloudy', 'wi wi-night-partly-cloudy', 'wi wi-day-sunny-overcast',
  'wi wi-night-clear', 'wi wi-day-sunny', 'wi wi-night-clear', 'wi wi-day-sunny', 'wi wi-rain-mix',
  'wi wi-hot', 'wi wi-thunderstorm', 'wi wi-thunderstorm', 'wi wi-thunderstorm', 'wi wi-rain', 'wi wi-snow',
  'wi wi-rain-mix', 'wi wi-snow', 'wi wi-cloud', 'wi wi-thunderstorm', 'wi wi-snow', 'wi wi-lightning']

function getIcon (value) {
  return _.get(iconArray, value, 'wi wi-na')
}

module.exports = { getIcon }
